<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'details' => $this->faker->paragraph(2),
            'image' => $this->faker->image('public/uploads/products', 740, 740, 'product', false),
            'buying_price' => $this->faker->randomFloat(2, 1, 100),
            'gst' => $this->faker->randomFloat(2, 0, 100),
            'profit_percentage' => $this->faker->randomFloat(2, 0, 100),
            'selling_price' => 10,
        ];
    }
}
