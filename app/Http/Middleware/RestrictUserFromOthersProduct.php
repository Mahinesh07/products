<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RestrictUserFromOthersProduct
{

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        $id = $request->route()->product->user_id;
        if ($id == Auth::id()) {
            return $next($request);
        }

        return redirect()->route('home');
    }
}
