<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductFormRequest;
use App\Models\Product;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{

    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        $this->middleware('restrict', ['only' => ['edit', 'update', 'show', 'delete']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $product = Product::where('user_id', Auth::id())->get();
            return Datatables::of($product)
                ->addIndexColumn()
                ->addColumn('action', function ($product) {
                    return '<a href="products/' . $product->id . '"><i class="fas fa-eye"></i></a>';
                })
                ->toJson();
        }

        return view('product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(): View|Factory|Application
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductFormRequest $request
     * @return RedirectResponse
     */
    public function store(ProductFormRequest $request): RedirectResponse
    {
        $filename = null;
        if ($request->hasfile('image')) {
            $filename = Product::uploadImage($request->file('image'));
        }
        $product = Product::create($request->all());
        $product->update([
            'image' => $filename,
        ]);

        return redirect()->route('products.show', $product->id);
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return Application|Factory|View
     */
    public function show(Product $product): View|Factory|Application
    {
        return view('product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @return Application|Factory|View
     */
    public function edit(Product $product): View|Factory|Application
    {
        return view('product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductFormRequest $request
     * @param Product $product
     * @return RedirectResponse
     */
    public function update(ProductFormRequest $request, Product $product): RedirectResponse
    {
        $filename = $product->image;
        if ($request->hasfile('image')) {
            $filename = Product::uploadImage($request->file('image'));
            $product->deleteOldImage();
        }
        $product->update($request->all());
        $product->update(['image' => $filename]);

        return redirect()->route('products.show', $product->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return RedirectResponse
     */
    public function destroy(Product $product): RedirectResponse
    {
        $product->deleteOldImage();
        $product->delete();

        return redirect()->route('products.index');
    }
}
