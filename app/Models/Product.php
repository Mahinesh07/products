<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\File;

/**
 * @method static create(array $all)
 * @property mixed id
 * @property mixed buying_price
 * @property mixed profit_percentage
 * @property mixed gst
 */
class Product extends Model
{

    use HasFactory;

    protected $guarded = [];

    /**
     * @param $file
     * @return string
     */
    public static function uploadImage($file): string
    {
        $extension = $file->getClientOriginalExtension();
        $filename = time() . '.' . $extension;
        $file->move('uploads/products/', $filename);

        return $filename;
    }

    /**
     * Deleting the old images
     */
    public function deleteOldImage()
    {
        $destination = 'uploads/products/' . $this->image;
        if (File::exists($destination)) {
            File::delete($destination);
        }
    }

    /**
     * @param $value
     * @return float|int|mixed
     */
    public function setSellingPriceAttribute($value): mixed
    {
        return $this->attributes['selling_price'] = $this->buying_price + $this->buying_price * $this->profit_percentage +
            $this->buying_price * $this->gst;
    }

    /**
     * @param $value
     * @return string
     */
    public function getNameAttribute($value): string
    {
        return ucfirst($value);
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
