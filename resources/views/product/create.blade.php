@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Create Product</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('products.index') }}"><i
                                            class="fas fa-home"></i></a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <x-alert/>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10">
                        <div class="card card-primary">
                            <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="text" name="user_id" value="{{ Auth::id() }}" class="form-control"
                                       hidden>
                                <div class="card-body">
                                    <div class="form-group">
                                        <strong>Name:</strong>
                                        <input type="text" name="name" class="form-control" placeholder="Enter Name">
                                    </div>
                                    <div class="form-group">
                                        <strong>Detail:</strong>
                                        <textarea type="text" name="details" class="form-control"
                                                  placeholder="Enter detail"> </textarea>
                                    </div>
                                    <div class="form-group">
                                        <strong>Buying Price:</strong>
                                        <input type="text" name="buying_price" class="form-control"
                                               placeholder="Enter Buying price">
                                    </div>
                                    <div class="form-group">
                                        <strong>GST:</strong>
                                        <input type="text" name="gst" class="form-control" placeholder="Enter GST">
                                    </div>
                                    <div class="form-group">
                                        <strong>Profit Percentage:</strong>
                                        <input type="text" name="profit_percentage" class="form-control"
                                               placeholder="Enter profit percentage">
                                    </div>
                                    <div class="form-group">
                                        <strong>Selling Price:</strong>
                                        <input type="text" name="selling_price" class="form-control"
                                               placeholder="Enter Selling price">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">File input</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="image">
                                                <label class="custom-file-label" for="exampleInputFile">Choose
                                                    file</label>
                                            </div>
                                            <div class="input-group-append">
                                                <span class="input-group-text">Upload</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
