@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Products</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('products.create')}}"><i
                                            class="fas fa-plus-circle" style="color:green;"></i></a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <div class="card-body">
            <table id="products-table" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Detail</th>
                    <th>Buying Price</th>
                    <th>GST</th>
                    <th>Profit Percentage</th>
                    <th>Selling Price</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@stop
@push('scripts')

    <script>
        $(function () {
            $('#products-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('products.index') !!}',
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'details', name: 'detail'},
                    {data: 'buying_price', name: 'buying_price'},
                    {data: 'gst', name: 'gst'},
                    {data: 'profit_percentage', name: 'profit_percentage'},
                    {data: 'selling_price', name: 'selling_price'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        });
    </script>
@endpush

