@extends('layouts.admin')

@section('content')
    <div class="hold-transition sidebar-mini">
        <div class="content-wrapper">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Show Product</h1>
                        </div>
                        <div class="col-sm-6">
                            <div class="float-md-right">
                                <a href="{{ route('products.index') }}"><i class="fas fa-home"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="content">

                <div class="card card-solid">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="col-12">
                                    <img src="{{ asset('uploads/products/'.$product->image) }}" height="400" width="550"
                                         alt="Product Image">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <h3 class="product-title my-3">{{ $product->name }}</h3>
                                <p class="product-description">{{ $product->details }}</p>
                                <hr>
                                <h4 class="mt-3">Buying Price: {{ $product->buying_price }} </h4>
                                <h4 class="mt-3">Profit Percentage: {{ $product->profit_percentage }}%</h4>
                                <div class="bg-gray py-2 px-3 mt-4">
                                    <h2 class="mb-0">
                                        Rs:{{ $product->selling_price }}
                                    </h2>
                                    <h4 class="mt-0">
                                        <small>GST Tax: {{ $product->gst }}% </small>
                                    </h4>
                                </div>
                                <div class="mt-3">
                                    <form action="{{ route('products.destroy',$product->id) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <a class="btn btn-primary" href="{{ route('products.edit', $product->id) }}">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <button type="submit" class="btn btn-danger ml-2">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

